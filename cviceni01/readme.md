Postup
======

1. stáhnout mapu ze stránky https://mapsvg.com/maps/czech-republic
2. vložit svg inline do html stránky do divu s id mapa nebo jiné
3. upravit barvu vykreslení

Vložit do svg:
`<style>
    path {
        fill:#b8b1b7;
        stroke:#ffffff
    }
               
</style>`

1. Zvýraznit kraj při najetí myši

přidat do stylu u svg:

`path:hover {
    fill: blueviolet
}`

5. Zobrazit informace na kliknutí myši nebo najetí

Přidat do svg prvku:
    `onclick="clickOnMap(event)"
    onmouseover="hoverOnMap(event)"`

Přidat do stránky script prvek s funkcemi:
`<script>

    const districtNames = new Map();
    districtNames.set("CZ-US", "Ústecký kraj");

    const displayDistrictName = (id) => {
        const name = districtNames.get(id);
        const divName = document.getElementById("districtName");
        divName.innerHTML = name ? name : id;
    }

    const clickOnMap = (event) => {
        const districtId = event.target.id;
        displayDistrictName(districtId)
    }

    const hoverOnMap = (event) => {
        const districtId = event.target.id;
        displayDistrictName(districtId)        
    }

</script>`


# Napojení na API
API je popsané na https://mapakriminality.docs.apiary.io

Statistiky jsou rozepsané podle oblastí. Je číselník oblastí, každá oblast má číslo úrovně. Kraje mají úroveň 1.
Každá oblast má kód (Code)

## Načtení seznamu krajů

Příkaz pro načtení seznamu krajů:

`curl -s https://www.mapakriminality.cz/api/areas | jq ".areas | .[] | select(.AreaLevel == 1) | {code: .Code, name: .Name}"`

Hodnotu Code si musí spojit k aplikaci k jednotlivým krajům.

{
  "code": "0000",
  "name": "KŘP HL. M. PRAHY"
}
{
  "code": "0100",
  "name": "KŘP STŘEDOČESKÉHO KRAJE"
}
{
  "code": "0200",
  "name": "KŘP JIHOČESKÉHO KRAJE"
}
{
  "code": "0300",
  "name": "KŘP PLZEŇSKÉHO KRAJE"
}
{
  "code": "0400",
  "name": "KŘP ÚSTECKÉHO KRAJE"
}
{
  "code": "0500",
  "name": "KŘP KRÁLOVÉHRADECKÉHO KRAJE"
}
{
  "code": "0600",
  "name": "KŘP JIHOMORAVSKÉHO KRAJE"
}
{
  "code": "0700",
  "name": "KŘP MORAVSKOSLEZSKÉHO KRAJE"
}
{
  "code": "1400",
  "name": "KŘP OLOMOUCKÉHO KRAJE"
}
{
  "code": "1500",
  "name": "KŘP ZLÍNSKÉHO KRAJE"
}
{
  "code": "1600",
  "name": "KŘP KRAJE VYSOČINA"
}
{
  "code": "1700",
  "name": "KŘP PARDUBICKÉHO KRAJE"
}
{
  "code": "1800",
  "name": "KŘP LIBERECKÉHO KRAJE"
}
{
  "code": "1900",
  "name": "KŘP KARLOVARSKÉHO KRAJE"
}

## Přehled kriminality
Přehled se vrací za určité období, je vhodné to oříznout nějakým námi daným obdobím, např. začátkem Covidu (3-2020).
Přehled je rozdělen podle typů zločinnosti. Bylo by dobré si vybrat jen některé typy nebo souhrn, ať to není moc složité.
Nebo se to dá později rozvíjet.

