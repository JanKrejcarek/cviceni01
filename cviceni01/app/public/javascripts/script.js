
let currentDistrictNode = null;
const dataMap = new Map();
const remote = document.URL.startsWith('http');
let currentAreaCode = "";

const displayDistrictName = (id, name) => {
   const divName = document.getElementById("districtName");
   divName.innerHTML = name ? name : id;
}

const showDistrict = (event) => {
   const districtId = event.target.id;
   const districtName = event.target.getAttribute('title');
   if (districtName) {
      displayDistrictName(districtId, districtName);
   }
}

const reload = (event) => {
   const year = event.target.value;
   
   const inputMonth = document.getElementById("mesic");
   let month = inputMonth.value;
      

   showProgress();
   getStatistics(currentAreaCode, year, month).then(crimes => {
      const table = generateTable(crimes);
      showTable(table);
   }).catch(error => {
      alert(error.message)
   })
}

const clickOnMap = (event) => {
   showDistrict(event);
   currentAreaCode = event.target.getAttribute('data-areacode')

   if (currentDistrictNode != null) {
      currentDistrictNode.setAttribute("class", '');
   }

   currentDistrictNode = event.target;
   currentDistrictNode.setAttribute("class", "highlighted");

   const inputYear = document.getElementById("rok");
   let year = inputYear.value;
   
   const inputMonth = document.getElementById("mesic");
   let month = inputMonth.value;
   

   showProgress();
   getStatistics(currentAreaCode, year, month).then(crimes => {
      const table = generateTable(crimes);
      showTable(table);      
      drawPieChart(crimes);
   }).catch(error => {
      alert(error.message)
   })
}

const hoverOnMap = (event) => {

}

const showTable = (table) => {
   const dataBody = document.getElementById('districtData');

   if (dataBody.firstChild) {
      dataBody.firstChild.replaceWith(table);
   } else {
      dataBody.appendChild(table);
   }
}

const showProgress = () => {
   const dataBody = document.getElementById('districtData');
   dataBody.innerHTML = "..."
}

const generateTableOld = (crimes) => {
   const table = document.createElement("table");

   const header = document.createElement("thead");
   header.innerHTML = '<tr><th>Období</th><th>Hlášených případů</th><th>Vyřešených případů</th></tr>';

   const body = document.createElement("tbody");
   crimes.forEach(crimeStats => {
      const row = document.createElement("tr");
      const rowData = `<td>${crimeStats.TimePeriod}</td><td>${crimeStats.Found}</td><td>${crimeStats.Solved}</td>`;
      row.innerHTML = rowData;

      body.appendChild(row);
   })

   table.appendChild(header);
   table.appendChild(body)

   return table;
}

const generateTable = (crimes) => {
   const table = document.createElement("table");

   const header = document.createElement("thead");
   header.innerHTML = '<tr><th>Typy zločinů</th><th>Hlášených případů</th><th>Vyřešených případů</th></tr>';

   const body = document.createElement("tbody");
   crimes.forEach(crimeStats => {
      const row = document.createElement("tr");
      
      if (crimeStats.CrimeType == "101-903") {
         crimeStats.CrimeType = "Všechny zločiny";
      }
      else if (crimeStats.CrimeType == "101-106") {
         crimeStats.CrimeType = "Vraždy";
      }
      else if (crimeStats.CrimeType == "311-390") {
         crimeStats.CrimeType = "Krádeže vloupáním";
      }
      else if (crimeStats.CrimeType == "411-490") {
         crimeStats.CrimeType = "Krádeže prosté";
      }
      else if (crimeStats.CrimeType == "611-690") {
         crimeStats.CrimeType = "Ostatní činy";
      }
      else if (crimeStats.CrimeType == "801-890") {
         crimeStats.CrimeType = "Hospodářské činy";
      } else if (crimeStats.CrimeType == "511") {
         crimeStats.CrimeType = "Podvod";
      }

      let rowData = `<td>${crimeStats.CrimeType}</td><td>${crimeStats.Found}</td><td>${crimeStats.Solved}</td>`;
      row.innerHTML = rowData;

      body.appendChild(row);
   })

   table.appendChild(header);
   table.appendChild(body)

   return table;
}

const loadStatistics = async (areaCode, year, month) => {
   

   //when run from filesystem we need to use a proxy (only for development purposes)
   let apiUrl = `api/crimes?areacode=${areaCode}&timefrom=${month}-${year}&timeto=${month}-${year}&crimetypes=511,101-106,311-390,411-490,611-690,801-890`;
   
   if (!remote) {
      apiUrl = `https://cors-anywhere.herokuapp.com/https://mapakriminality.cz/${apiUrl}`
   } else {
      apiUrl = `/.netlify/functions/${apiUrl}`
   }
      
   const response = await fetch(apiUrl);
   if (response.body !== null) {
      const data = await response.json();
      return data.crimes;
   } else {
      throw {message: "no response received"}
   }

}

const getStatistics = async (areaCode, year, month) => {
   
   const key = `${areaCode}#${year}#${month}`;
   
   if (dataMap.has(key)) {
      console.log(`Cache hit for ${key}`)
      return dataMap.get(key)
   } else {
      console.log(`Cache miss for ${key}`)
      const newData = await loadStatistics(areaCode, year, month);
      dataMap.set(key, newData);

      return newData;
   }
}

const drawCrimesChart = (crimes) => {
   google.charts.load('current', {packages: ['corechart', 'line']});
   google.charts.setOnLoadCallback(drawChartX);

   function drawChartX() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'TimePeriod');
      data.addColumn('number', 'Found cases');

      const rawData = crimes.map(crime => [crime.TimePeriod, crime.Found]);
      
      
      //var data = google.visualization.arrayToDataTable(rawData);
      data.addRows(rawData)


      var options = {
        hAxis: {
          title: 'Time'
        },
        vAxis: {
          title: 'Popularity'
        },
        backgroundColor: '#f1f8e9'
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart'));
      chart.draw(data, options);
   }
}



const drawPieChart = (crimes) => {
  
   google.charts.load('current', { 'packages': ['corechart'] });
   google.charts.setOnLoadCallback(drawChart);  //volá interní funkci níže

   function drawChart() {  //zajišťuje vykreslení grafu
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'TimePeriod');
      data.addColumn('number', 'Found cases');
   
      let rowData = []; //obsahuje údaje odpovídající dvěma sloupcům výše

      //převod kódu na název zločinu
      crimes.forEach(crimeStats => {                  
         if (crimeStats.CrimeType == "101-903") {
            crimeStats.CrimeType = "Všechny zločiny";
         }
         else if (crimeStats.CrimeType == "101-106") {
            crimeStats.CrimeType = "Vraždy";
         }
         else if (crimeStats.CrimeType == "311-390") {
            crimeStats.CrimeType = "Krádeže vloupáním";
         }
         else if (crimeStats.CrimeType == "411-490") {
            crimeStats.CrimeType = "Krádeže prosté";
         }
         else if (crimeStats.CrimeType == "611-690") {
            crimeStats.CrimeType = "Ostatní činy";
         }
         else if (crimeStats.CrimeType == "801-890") {
            crimeStats.CrimeType = "Hospodářské činy";
         }

         //jeden řádek (a výseč v grafu)
         let dataPie = [crimeStats.CrimeType, crimeStats.Found];
         
         //řádek se vloží do pole rowData z řádku 226
         rowData.push(dataPie);
      });
   
      data.addRows(rowData)   //všechny řádky (výseče) se přidají do datové tabulky pro graf

      var chart = new google.visualization.PieChart(document.getElementById('chart')); //graf bude vložen na stránku do prvku s id 'chart'
      chart.draw(data);
   }

}

   