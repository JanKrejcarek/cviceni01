var express = require('express');
var router = express.Router();

const axios = require('axios');

router.get('*', async function(req, res, next) {
              
    const base = `https://www.mapakriminality.cz/api`
    
    console.log(JSON.stringify(req.query))

    const options = {
        method: 'get',
        url: `${base}${req.path}`,
        params: req.query
    }

    console.log(JSON.stringify(options))

    try {
        const response = await axios(options);    
        res.json(response.data);
    } catch (error) {
        res.status(error.response.status);
    }

});

module.exports = router;
