# Fraud statistics for selected district since March 2020 in the Czech Republic

This is a school project on HTML, CSS and JavaScript programming.

This web app displays a map of the Czech Republic with districts. 
After clicking on a district, the app downloads monthly fraud crime statistics
for that district since March 2020 and displays it as a table below the map.

It is not meant to be run as a real web application.

## To run the app locally
1. Install Node.js
2. Switch to the app folder and run ``npm install``
3. Type ``npm start`` to run the Express server
4. Navigate your browser to http://localhost:3000
